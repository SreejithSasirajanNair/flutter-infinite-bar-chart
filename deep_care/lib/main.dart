import 'dart:math';

import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/rendering.dart';
import 'package:intl/intl.dart';
import 'package:charts_common/common.dart' as common;

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  DateTime now = DateTime.now();
  var formatter = DateFormat('yyyy-MM-dd');
  var displayFormatter = DateFormat('EEE dd, MM');
  final ScrollController scrollerController = ScrollController();
  List<Data> listData = [];
  List<common.AnnotationSegment<Object>> topLabels = [];
  List<charts.TickSpec<String>> bottomLabels = [];
  DateTime? firstDate;
  double width = 1100;
  var rnd = Random();
  bool isLoading = false;
  bool firstScroll = true;

  @override
  void initState() {
    super.initState();
    // Load initial data
    _loadInitialData();
    // Extra markings for labels
    listData.add(Data('0',0,0,0,0,0));
    bottomLabels.add(
      const charts.TickSpec(
          '0',
          label: 'MET\nJogging',
          style: charts.TextStyleSpec(
              lineHeight: 2
          )
      )
    );
    topLabels.add(
      charts.LineAnnotationSegment(
        70,
        charts.RangeAnnotationAxisType.measure,
        dashPattern: [2,2],
        color: charts.MaterialPalette.blue.shadeDefault,
        endLabel: 'Ziel')
    );

    // Jump the scroller to the end
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      scrollerController.jumpTo(scrollerController.position.maxScrollExtent);
    });

  }

  _MyHomePageState();

  @override
  Widget build(BuildContext context) {
    // Chart series
    List<charts.Series<Data, String>> timeline = [
      charts.Series(
        id: 'Green',
        fillColorFn: (_, __) => charts.MaterialPalette.green.shadeDefault.lighter,
        data: listData,
        domainFn: (Data data, _) => data.date,
        measureFn: (Data data, _) => data.green,
      ),
      charts.Series(
        id: 'Pink',
        fillColorFn: (_, __) => charts.MaterialPalette.pink.shadeDefault.lighter,
        data: listData,
        domainFn: (Data data, _) => data.date,
        measureFn: (Data data, _) => data.pink,
      ),
      charts.Series(
        id: 'Blue',
        fillColorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault.lighter,
        data: listData,
        domainFn: (Data data, _) => data.date,
        measureFn: (Data data, _) => data.blue,
      )
    ];

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      // Notification listener to listen scroll events
      body: NotificationListener<ScrollNotification>(
          onNotification: (scrollNotification) {
            if(scrollNotification is UserScrollNotification){
              if (scrollNotification.direction == ScrollDirection.forward) {
                if(scrollNotification.metrics.atEdge) {
                  if(!firstScroll) {
                    setState((){
                      var dateTmp = DateTime(firstDate!.year, firstDate!.month, firstDate!.day);
                      _loadMoreData(dateTmp);
                      width += 1000;
                    });
                  }
                  firstScroll = false;
                }
              }
            }
            return true;
          },
          child: SingleChildScrollView(
          controller: scrollerController,
              scrollDirection: Axis.horizontal,
              child: Container(
                  height: 300,
                  width: width,
                  padding: const EdgeInsets.only(top: 20, bottom: 20, left: 0, right: 0),
                  child: Stack(
                    children: [
                      isLoading? const SizedBox(height: 300, width: 50, child: Center(child: CircularProgressIndicator())) : Row(),
                      charts.BarChart(
                        timeline,
                        animate: true,
                        primaryMeasureAxis: const charts.NumericAxisSpec(renderSpec: charts.NoneRenderSpec()),
                        domainAxis: charts.OrdinalAxisSpec(
                            tickProviderSpec:
                            charts.StaticOrdinalTickProviderSpec(bottomLabels)),
                        behaviors: [
                          charts.RangeAnnotation(topLabels),
                        ],
                      ),
                    ],
                  )
              )
          )
      )
    );
  }

  void _loadInitialData() {
    for(int i = 9; i >= 0; i--) {
      var newDate = DateTime(now.year, now.month, now.day - i);
      int met = rnd.nextInt(100);
      double jogging = rnd.nextDouble()* 10;
      listData.add(Data( formatter.format(newDate), rnd.nextInt(100),rnd.nextInt(100),rnd.nextInt(100), met, jogging));
      topLabels.add(
          charts.RangeAnnotationSegment(
            formatter.format(newDate),
            formatter.format(newDate),
            charts.RangeAnnotationAxisType.domain,
            middleLabel: displayFormatter.format(newDate),
            labelAnchor: charts.AnnotationLabelAnchor.end,
            color: charts.MaterialPalette.gray.shade200,
            labelPosition: charts.AnnotationLabelPosition.margin,
            labelDirection: charts.AnnotationLabelDirection.horizontal,
          )
      );
      bottomLabels.add(
        charts.TickSpec(
            formatter.format(newDate),
            label: '$met\n${jogging.toStringAsFixed(1)}',
            style: const charts.TextStyleSpec(
                lineHeight: 2
            )
        ),
      );

      if(i == 9) {
        firstDate = newDate;
      }
    }
  }

  void _loadMoreData(dateTmp) async{
    setState(() {
      isLoading = true;
    });

    for(int i = 0; i < 10; i++) {
      var newDate = DateTime(dateTmp.year, dateTmp.month, dateTmp.day - (10-i));
      int met = rnd.nextInt(100);
      double jogging = rnd.nextDouble()* 10;
      listData.insert(i, Data( formatter.format(newDate), rnd.nextInt(100),rnd.nextInt(100),rnd.nextInt(100), met, jogging));

      topLabels.insert(i,
          charts.RangeAnnotationSegment(
              formatter.format(newDate),
              formatter.format(newDate),
              charts.RangeAnnotationAxisType.domain,
              middleLabel: displayFormatter.format(newDate),
              labelAnchor: charts.AnnotationLabelAnchor.end,
              color: charts.MaterialPalette.gray.shade200,
              labelPosition: charts.AnnotationLabelPosition.margin,
              // Override the default vertical direction for domain labels.
              labelDirection: charts.AnnotationLabelDirection.horizontal)
      );
      bottomLabels.add(
        charts.TickSpec(
            formatter.format(newDate),
            label: '$met\n${jogging.toStringAsFixed(1)}',
            style: const charts.TextStyleSpec(
                lineHeight: 2
            )
        ),
      );

      if(i == 0) {
        firstDate = newDate;
      }
    }

    await Future.delayed(const Duration(milliseconds: 2000));
    setState(() {
      isLoading = false;
    });
  }

}

class Data {
  final String date;
  final int green;
  final int pink;
  final int blue;
  final int met;
  final double jogging;

  Data(this.date, this.green, this.pink, this.blue, this.met, this.jogging);
}
